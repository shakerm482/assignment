function isLoggedIn() {
  const token = localStorage.getItem('APP_TOKEN');

  if (token) {
    return true;
  }

  return false;
}

if (isLoggedIn()) {
  location.replace('/app.html');
} else {
  location.replace('/login.html');
}

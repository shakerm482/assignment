if (!localStorage.getItem('APP_TOKEN')) {
  location.replace('/login.html');
}

var myButton = document.querySelector('.out');
myButton.onclick = function () {
  localStorage.removeItem('APP_TOKEN');
  location.replace('/login.html');
};





/* to do list*/
var myElement = document.querySelector('.tasks'),
    myVal = document.querySelector('.input');

document.querySelector('.add').onclick = function () {
  var  myNewTag = document.createElement('div'),
       myInput = document.createElement('input'),
       mySpan = document.createElement('span'),
       myAside = document.createElement('aside'),
       times = document.createTextNode('X');

  myInput.setAttribute('type', 'checkbox');
  mySpan.innerHTML = myVal.value;
  myAside.appendChild(times);
  myNewTag.appendChild(myInput);
  myNewTag.appendChild(mySpan);
  myNewTag.appendChild(myAside);
  myElement.appendChild(myNewTag);
  myVal.value = '';

  myAside.onclick = function(){
     this.parentElement.remove();
  };

/* checkbox */
  myInput.onclick = function () {
    if(this.nextElementSibling.style.textDecoration !=  'line-through') {
      this.nextElementSibling.style.textDecoration = 'line-through';
    } else {
      this.nextElementSibling.style.textDecoration = 'none';
    }
  };


/* search box */
};
var searchBox = document.querySelector('.search');
searchBox.onkeyup = function () {
  var filter = searchBox.value.toUpperCase(),
      created = myElement.getElementsByTagName('div');
for (var i = 0; i < created.length; i++) {
  var myDiv = created[i].getElementsByTagName('span')[0],
      textValue = myDiv.textContent;

  if (textValue.toUpperCase().indexOf(filter) > -1) {
    created[i].style.display = 'block';
  } else {
    created[i].style.display = 'none';
    }
  }
 };
